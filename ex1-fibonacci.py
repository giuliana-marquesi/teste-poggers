def fibonacci(n):
	x = 1
	y = 0
	for i in range(n):
		x,y = y, y+x
	return y