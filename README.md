# RESOLUÇÂO DOS EXERCÌCIOS PROPOSTOS #

## PARTE 1 ##

### Exercício 1 ###

O uso recursão simples para resolver este caso não é uma boa saída por alocar muita memória caso o *enésimo número* seja muito alto e também repetir contas já computadas diversas vezes. Existem alternativas dentro da recursão como como a recursão de calda ou o uso de pilhas para controlar a memória e as contas já feitas, mas não tenho experiência com estes algoritmos, decidi ir pelo certo, neste momento, com uma iteração.

Resolução:
	
	def fibonacci(n):
		x = 1
		y = 0
		for i in range(n):
			x,y = y, y+x
		return y
		
Como uma alternativa para a resolução recursiva encontrei o decorador *lru_cache* na linguagem python que deixa em cache certos resultados de operações para não tê-las de repití-las, salvando em partes o gasto da memória, como no exemplo a seguir:

	from functools import lru_cache

	@lru_cache(maxsize=50)
	def fibonacci(n):
		if n < 2:
			return n
		return fibonacci(n -1) + fibonacci(n - 2)

### Exercício 2 ###

Considerando que a função *imprima* não pule linha, serão impressos os números e suas respectivas palavras, se necessário, e como última instrução dentro do loop será pulada uma linha.

O código de resolução segue abaixo:


	inteiro i = 0
	enquanto(i< 100) faça
		imprima(++i);
		se i % 15 == 0
		então
			imprima("BUZZBIZZ");
		senão
			se i % 3 == 0
			então
				imprima("BUZZ")
			fim se
			se i % 5 == 0
			então
				imprima("BIZZ")
			fim se
		fim se
		imprima(pula_linha)
	fim enquanto

### Exercício 3 ###

É possível resolver esta questão usando recursos embutidos da linguagem python como no caso:

	def une_ordena(A,B):
		C = A + B
		C.sort()
		set(C)

Porém, é sabido que as estruturas de dados usadas nesta resolução são lista e set, cada uma com sua propriedade particular, consideradas como conjunto de dados, podendo assim, não ser considerado um array "puro". Além disso,  como é um teste talvez seja esperado uma implementação mais crua para entender meu raciocício lógico. 

No caso da minha resposta escolhi fazer a junção dos dois vetores, aqui listas, ordenando minimamente enquanto os unia em outro novo vetor. Caso algum deles seja maior que outro, decidi fazer uma cópia do excedente direto para o novo vetor. Em seguida, usei o algoritmo *insertion sort* por já termos um vetor previamente ordenado, onde ele funciona de maneira eficiente.

Segue a resolução:

	def insertion_sort(lista):
		for i in range(1, len(lista)):
			chave = lista[i]
			k = i
			while k > 0 and chave < lista[k -1]:
				lista[k] = lista[k -1]
				k -= 1
			lista[k] = chave
			
	def une_ordena(A,B):
		C = []
		if len(A) < len(B):
			menor = A
			maior = B
			dif = len(B) - len(A)
		else:
			menor = B
			maior = A
			dif = len(A) - len(B)
		
		for i in range(len(menor)):
			if menor[i] == maior[i]:
				C.append(menor[i])
			if menor[i] < maior[i]:
				C.append(menor[i])
				C.append(maior[i])
			else:
				C.append(maior[i])
				C.append(menor[i])

		C += maior[dif:]
		
		insertion_sort(C)
			
		return C

## PARTE 2 ##

Para este exercício, pesquisei os serviços da Amazon que nunca cheguei a usar de fato, logo, achei muitos diagramas de exemplo com o que é pedido na questão. Desenhei um de acordo com o que acredito ser uma boa prática e ainda sim, mantendo a simplicidade da arquitetura.

O uso do loadbalancer em conjunto com várias instâncias da aplicação garantem uma melhor disponibilidade e garantindo que nao fique fora do ar. A replicação do banco de dados garante um backup, caso falhe o banco de dados principal.

![Resolução exercício infraestrutura](Ex4-Alta fiabilidade.png)

## PARTE 3 ##

A melhor forma de lidar com esta situação seria estudar a aplicação legada em questão e o que ficou obsoleto nela, apenas bibliotecas? Foram mudadas funções ou requisitos? Qual seria o nível de depreciação ou de qual tipo, a aplicação em seu alto nível ou seu código e dependência? Depois de levantar resultado deste estudo é preciso saber o que pode ser mantido ou o que não necessita de urgência. Sabendo-se do que se quer modificar e ser mantido, há o outro passo, da refatoração.

Existem princípios bem estudados e amplamente testados de como refatorar um projeto e também manter código legado, dentre eles o livro ["Refatoração: Aperfeiçoando o projeto de código existente"](https://www.refactoring.com/catalog/) de Martin Fowler. Seguir tais princípios torna-se boa prática. Dentre esses princípios: Observar a comunicação entre partes do código e preocupar-se em mante-las funcionando ao construir pequenas alterações temporárias que entregam a comunicação necessária para o novo código, mas sem extinguir abruptamente a outra forma de comunicação. Isto se dá tanto adicionando novos parâmetros ou construindo dispatchers.

Para evitar este nível de depreciação de  código o uso do princípio SOLID que nos atenta para escrever códigos menos acoplados sempre se questionando da responsabilidade de cada parte do código aliado à prática de microserviços que não prende o código à um monolito escrito em apenas uma linguagem com uma estrutura fechada talvez seja uma boa pratica atual. Outro recurso que pode facilitar na refatoração e atenção às responsabilidades de um requisito são os testes.

## OBS ##

Para executar os códigos em python basta acessar a pasta que contém eles e digitar:

	python3 -i nome-arquivo.py

para entrar em modo iterativo. O método escrito já estará carregado e poderá ser usado, como:

	fibonacci(70)
	import random
	A = [random.randint(0,10000) for x in range(15)]
	B = [random.randint(0,10000) for x in range(15)]
	une_ordena(A,B)

	une_ordena(A,B)

